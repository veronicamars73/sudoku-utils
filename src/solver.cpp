// Programa de impressão de texto.

#include <iostream> // permite que o programa gere saída de dados na tela
#include "../include/Resolve.h"

int main(){
  int i, j;
  /*int tab[SIZE][SIZE]={{5,3,0,0,7,0,0,0,0},
                  {6,0,0,1,9,5,0,0,0},
                  {0,9,8,0,0,0,0,6,0},
                  {8,0,0,0,6,0,0,0,3},
                  {4,0,0,8,0,3,0,0,1},
                  {7,0,0,0,2,0,0,0,6},
                  {0,6,0,0,0,0,2,8,0},
                  {0,0,0,4,1,9,0,0,5},
                  {0,0,0,0,8,0,0,7,9}}; */
  int tab[SIZE][SIZE];
  //int numerodesolucoes{0};
  // Print do tabuleiro
  while (!(std::cin.eof())) {
    for (i = 0; i < SIZE; i++) {
      for (j = 0; j< SIZE; j++){
        std::cin >> tab[i][j];
      }
    }
    // Solução
    printTab(tab);
    resolve_tabuleiro(tab);
    // print solução
    std::cout << "Solução do ";
    printTab(tab);
    //std::cout << numerodesolucoes << " soluções foram encontradas no tabuleiro.\n";
  }
  
} // fim da função main

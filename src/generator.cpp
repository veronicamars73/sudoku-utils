// Programa de impressão de texto.
#include <random>
#include <algorithm>
#include <iostream>
#include "Resolve.h"
#include <time.h>



int main(){
  int i, j, cont{0};
  int tab[SIZE][SIZE];
  // Print do tabuleiro
  std::random_device rd;
  std::mt19937 g(rd());
  int possibilidades[9]={1,2,3,4,5,6,7,8,9};
  std::shuffle(std::begin(possibilidades), std::end(possibilidades), g);

  for(i=0; i<3; i++){
    for(j=0;j<3; j++){
      tab[i][j] = possibilidades[cont];
      cont=cont+1;
    }
  }
  std::shuffle(std::begin(possibilidades), std::end(possibilidades), g);
  cont = 0;

  for(i=6; i<9; i++){
    for(j=3;j<6; j++){
      tab[i][j] = possibilidades[cont];
      cont=cont+1;
    }
  }

  std::shuffle(std::begin(possibilidades), std::end(possibilidades), g);
  cont = 0;

  for(i=3; i<6; i++){
    for(j=6;j<9; j++){
      tab[i][j] = possibilidades[cont];
      cont=cont+1;
    }
  }

  /*srand(time(0));
  j = rand()%8;

  for(i = 0; i < SIZE; i++){
    tab[i][j] = possibilidades[i];
  }*/
  printTab(tab);
  // Solução
  resolve_tabuleiro(tab);
  // print solução
  std::cout << "Solução do ";
  printTab(tab);
  return 0; // indica que o programa terminou com sucesso
} // fim da função main

# Introduction

This program intends to be a sudoku puzzles **solver** and **generator**. Alongside a library of sudoku related algorithms, the two parts of the project work in two different running script to create a new and random puzzle and solve any number of puzzles.


# Solver

The solver programs runs using the "biblioteca_sudoku.cpp" and "solver.cpp" files, in order to solve any puzzle given.

The puzzles can be given using a input file like the example in the _input_ directory or a console manual input.


# Generator

The generator intends to create a random one solution puzzle and print on the console.

# Compiling

This project may be compiled with [CMake](https://cmake.org).

To compile this project with [cmake](https://cmake.org) follow these steps:

1. `cd sudoku-utils`: enters into the main project directory.
2. `cmake -S . -Bbuild`:  asks cmake to create the build directory and generate the Unix Makefile based on the script found in `CMakeLists.txt`, on the current level.
3. `cd build`: to enter the build folder.
5. `cmake --build .` or `make`: to trigger the compiling process.

The executables are created inside the `build` directory.

# Running

From the root folder, run as usual (assuming `$` is the terminal prompt):

To run the solver program using a input file:

```
$ ./build/solver < input/input.txt
```

To run the solver program without a input file:

```
$ ./build/solver
```

And to run the generator program:

```
$ ./build/generator
```

# Authorship

Program developed by Lisandra Melo (<mendie73@gmail.com>) as a project for the Linguagem de Programação 2020.1 class.

&copy; IMD/UFRN 2020.
